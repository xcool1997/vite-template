import vue from "@vitejs/plugin-vue";

import createAutoImport from "./autoImport"; // 自动引入
import createCompression from "./compression"; // gzip压缩
// import createSvgIcon from './svg-icon'
// import createSetupExtend from './setup-extend'

export default function createVitePlugins(viteEnv, isBuild = false) {
  const vitePlugins = [vue()];
  vitePlugins.push(createAutoImport());
  isBuild && vitePlugins.push(...createCompression(viteEnv));
  // vitePlugins.push(createSetupExtend())
  // vitePlugins.push(createSvgIcon(isBuild))
  return vitePlugins;
}
