// 动态图片路径
function getAssetsImg(url) {
  return new URL(`/src/assets/images/${url}`, import.meta.url).href;
}

// 通过插件方式挂载全局方法
export default {
  install(app) {
    app.config.globalProperties.$getAssetsImg = getAssetsImg;
  },
};
