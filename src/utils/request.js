import axios from "axios";

axios.defaults.headers["Content-Type"] = "application/json;charset=utf-8";
// 环境打印
console.log(import.meta.env);
console.log(import.meta.env.VITE_BASE_API);

const instance = axios.create({
  baseURL: import.meta.env.VITE_BASE_API,
  timeout: 30000,
});

// 拦截器
instance.interceptors.request.use(
  (config) => {
    config.headers["Authorization"] = `Bearer ${
      localStorage.getItem("token") || ""
    }`;
    return config;
  },
  (error) => Promise.reject(error)
);

// 响应器
instance.interceptors.response.use(
  (response) => {
    // 二进制数据直接返回
    if (
      response.request.responseType === "blob" ||
      response.request.responseType === "arraybuffer"
    ) {
      return response.data;
    }

    if (response.status === 200) {
      return Promise.resolve(response.data);
    }
    return Promise.reject(new Error(response.message || "Error"));
  },
  (error) => {
    if (error.response.status === 401) {
      console.log("token失效");
    }
    return Promise.reject(error.response);
  }
);

export default instance;
