export default defineStore("counter", {
  state: () => ({
    counter1: 1,
    counter2: 3,
    counter3: 5,
  }),
  getters: {
    doubleCount: (state) => state.counter1 * 2,
  },
  actions: {
    increment() {
      this.counter1++;
      this.counter2++;
      this.counter3++;
    },
  },
  // 开启持久化
  persist: {
    enabled: true, // 启用
    strategies: [
      // storage:可选localStorage/sessionStorage，paths:给指定数据持久化
      {
        key: "counter",
        storage: localStorage,
        paths: ["counter1", "counter2"],
      },
    ],
  },
});
