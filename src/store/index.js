import piniaPersist from "pinia-plugin-persist";

const store = createPinia();

store.use(piniaPersist); // 数据持久化

export default store;
