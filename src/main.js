import { createApp } from "vue";
import App from "./App.vue";
import element_plus from "element-plus";
// 引入路由
import router from "./router";
// pinia
import store from "./store";
// 全局方法
import global from "@/utils/global";

const app = createApp(App);
// 通过插件方式挂载全局方法
app.use(global);
// 全局组件挂载
// app.component('comp', comp)
app.use(store);
app.use(router);
app.use(element_plus);

app.mount("#app");
