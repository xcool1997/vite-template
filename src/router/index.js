// index.js

// 引入需要的模块
import { createRouter, createWebHashHistory } from "vue-router";

// 下面使用了es6的对象增强写法，命名必须是routes
const routes = [
  {
    path: "/",
    redirect: "/example",
  },
  {
    path: "/home",
    component: () => import("@/views/home/Index"),
  },
  {
    path: "/example",
    component: () => import("@/views/example/example.vue"),
  },
];

// 创建路由
const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// 导出路由
export default router;
