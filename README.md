## 安装依赖
```
npm install
```
## 运行

### 本地环境

```
npm run serve
```
### 本地环境
```
npm run start:dev
```

### 测试环境
```
npm run start:release

```

## 打包
```
npm run build
```

# 使用 vite 自动引入插件：unplugin-auto-import

# 数据持久化：pinia-plugin-persist

# gzip压缩：vite-plugin-compression

# 参考文章：https://zhuanlan.zhihu.com/p/441467856

# 移动端UI组件库

> varlet: https://varlet.gitee.io/varlet-ui/#/zh-CN/home
> nutui: https://nutui.jd.com/#/guide/intro
> vant: https://vant-contrib.gitee.io/vant/#/zh-CN (swipe轮播在ios微信h5中会存在闪烁问题)

# pc端UI组件库

> element-plus: https://element-plus.gitee.io/zh-CN/guide/design.html
> Ant Design of Vue: https://www.antdv.com/docs/vue/introduce-cn

# setup兼容处理
```js
<script>
    export default {
        name:'tree',
        inheritAttrs: false,
    }
</script>
```
