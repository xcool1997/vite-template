// vite.config.js
import { defineConfig, loadEnv } from "vite";
import path from "path";
// const { proxyUrl } = require("./src/config");

import createVitePlugins from "./vitePlugins"; // vite插件

const console = require("console");

export default defineConfig(({ mode, command }) => {
  console.log(mode); // 构建环境develop/production
  console.log(command); // 执行命令行serve/build，serve启动，build构建
  const env = loadEnv(mode, process.cwd());
  console.log(env); // {VITE_APP_TITLE: 'xx管理系统',VITE_BASE_API: 'https://xxx.com/api'}
  return {
    plugins: createVitePlugins(env, command === "build"),
    lintOnSave: false,
    resolve: {
      // 配置路径别名
      alias: {
        "@": path.resolve(__dirname, "src"),
      },
      // 省略文件后缀
      extensions: ["", ".js", ".json", ".vue", ".scss", ".css"],
    },
    server: {
      // 修改端口
      port: 5000,
      host: "0.0.0.0",
      // proxy: {
      //   "/api": {
      //     target: proxyUrl,
      //     changeOrigin: true,
      //     logLevel: "debug",
      //     pathRewrite: {
      //       "^/api": proxyUrl, // 路径重写
      //     },
      //     cookieDomainRewrite: "localhost",
      //   },
      // },
    },
    build: {
      target: "es2020", // 动态图片必须配置
      minify: "terser",
      terserOptions: {
        compress: {
          //生产环境时移除console
          drop_console: true,
          drop_debugger: true,
        },
      },
    },
    optimizedeps: {
      // 动态图片必须配置
      esbuildoptions: {
        target: "es2020",
      },
    },
  };
});
